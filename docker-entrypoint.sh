#!/bin/bash -e

service boinc-client start
sleep 2
boinccmd --project_attach ${boincurl} ${boinckey}
sleep 2

exec $@
