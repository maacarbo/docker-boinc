#!/bin/bash -e

BOINC_URL="www.worldcommunitygrid.org"
BOINC_KEY="911479_e42f737d7c28a11b31eafe58cbf10dd4"

NAME="boinc"
TAG="maacarbo/boinc"

# Pull image
docker pull ${TAG}

# Remove running container if any
docker rm -f ${NAME} || true

# Run it!
docker run \
	-d \
	--name ${NAME} \
	--rm \
	-h ${NAME}-$(hostname -s) \
	-e boincurl=${BOINC_URL} \
	-e boinckey=${BOINC_KEY} \
	--privileged \
	${TAG}

docker logs -f ${NAME}
