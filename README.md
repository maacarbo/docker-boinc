# BOINC

[BOINC](https://boinc.berkeley.edu/) lets you help cutting-edge science research using your computer (Windows, Mac, Linux) or Android device. BOINC downloads scientific computing jobs to your computer and runs them invisibly in the background. It's easy and safe.

[Join World Community Grid today!](https://join.worldcommunitygrid.org?recruiterId=911479)

## Multi arch

* `amd64`
* `arm64v8`
* `arm32v7`

## How to use
This is an example on how to use it:
```shell
docker run \
  -d \
  --name boinc \
  --rm \
  -h boinc-$(hostname -s) \
  -e boincurl=${BOINC_URL} \
  -e boinckey=${BOINC_KEY} \
  --privileged \
  --cap-add=SYS_NICE \
  maacarbo/boinc
```

where `BOINC_URL` is the project URL and `BOINC_KEY` your personal key.

## How to build

Make your system multi arch build ready:
```shell
docker run --rm --privileged multiarch/qemu-user-static:register --reset
```
_Restart docker_ and run [`build.sh`](build.sh).
