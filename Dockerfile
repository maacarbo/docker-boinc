FROM debian:10-slim

RUN apt-get update \
 && apt-get install --no-install-recommends -y boinc procps \
 && apt-get autoremove -y && apt-get clean && rm -rf /var/lib/apt/lists/*

COPY docker-entrypoint.sh /usr/local/bin/docker-entrypoint.sh
RUN chmod +x /usr/local/bin/docker-entrypoint.sh

ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["tail","-f","/var/lib/boinc-client/std*.txt"]
